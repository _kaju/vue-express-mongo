export default function ({route, redirect, app, store}) {
  const hasAuthCookie = document.cookie.indexOf('authorization=') > -1
  if (hasAuthCookie) {
    // authenticated - only redirect in case login page is accessed
    if (route.path === '/login') {
      return redirect('/')
    }
  } else {
    // not authenticated, send to login
    if (route.path !== '/login' && route.path !== '/signup') {
      return redirect('/login')
    }
  }
}
