import jwt from 'jsonwebtoken'

const defaultState = {
  user: {},
  loading: false,
  errorMessage: ''
}

export const state = () => defaultState

export const mutations = {
  startLogin (state) {
    state.loading = true
    state.errorMessage = ''
  },
  loginSuccess (state) {
    state.loading = false
  },
  loginFailed (state, errorMessage) {
    state.loading = false
    state.errorMessage = errorMessage
  },
  setError (state, errorMessage) {
    state.errorMessage = errorMessage
  }
}

export const actions = {
  login ({commit}, payload) {
    commit('startLogin')
    const {$axios, $router} = this
    const {username, password} = payload
    $axios.post('/api/auth/login', payload).then((res) => {
      commit('loginSuccess')
      $router.replace({path: '/'})
    }).catch((err) => {
      commit('loginFailed', err.response.data.message)
    })
  },
  logout () {
    const {$axios, $router} = this
    $axios.get('/api/auth/logout').then((res) => {
      $router.replace({path: '/login'})
    })
  }
}
