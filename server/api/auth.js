const express = require('express')
const router = express.Router()
const _ = require('lodash')
const moment = require('moment')
const jwt = require('jsonwebtoken')
const requireAuth = require('../middleware/requireAuth')
const User = require('../models/user')

router.post('/login', async function (req, res) {
  const {email, password} = req.body
  if (email && password) {
    // find user in db
    return User.findOne({email}, ['email', 'password'], async function (err, user) {
      if (err) return res.status(401).send({message: req.__('errors.dbError')})
      if (user) {
        // compare password
        const isValidPassword = await user.isValidPassword(password)
        if (!isValidPassword) return res.status(401).send({message: req.__('errors.wrongCreds')})

        // generate auth cookie
        const now = moment()
        // token expiry X hours from now
        const sessionExpires = moment().add(process.env.AUTH_COOKIE_EXPIRY_TIME, 'hours')
        // maxAge in seconds
        const maxAge = sessionExpires - now
        // if ssl is provided we set the cookie to secure
        const secure = process.env.HAS_HTTPS === 'true'
        // httpOnly needs to be false so we can access the token with client side javascript to assume we're logged in. Backend routes are locked down anyways
        const httpOnly = false
        // generate token
        const token = await jwt.sign({email}, process.env.JWT_SECRET, {expiresIn: maxAge})
        // attach auth cookie and send response
        const cookieOptions = {
          maxAge,
          secure,
          httpOnly,
          sameSite: 'strict'
        }
        return res.cookie('authorization', token, cookieOptions).status(200).send()
      } else {
        // no user found with these details.
        return res.status(401).send({message: req.__('errors.wrongCreds')})
      }
    })
  } else {
    // no email or password supplied
    return res.status(401).send({message: req.__('errors.missingCreds')})
  }
})

router.get('/logout', requireAuth, function (req, res) {
  // remove cookie and send response
  return res.clearCookie('authorization').status(200).send()
})

module.exports = router
