const express = require('express')
const router = express.Router()
const requireAuth = require('../middleware/requireAuth')

const authRoute = require('./auth')
const usersRoute = require('./users')

router.get('/', function (req, res) {
  res.send('Hi from api')
})

router.use('/auth', authRoute)
router.use('/users', usersRoute)

module.exports = router
