const express = require('express')
const router = express.Router()
const User = require('../models/user')

router.post('/', function (req, res) {
  const {email, password} = req.body
  if (email && email.length > 0 && password && password.length > 0) {
    // check if there is a user with that email
    return User.findOne({email}, 'email', async function (err, user) {
      if (err) return res.status(400).send({message: req.__('errors.dbError')})
      // user exists, send back errormessage
      if (user) return res.status(400).send({message: req.__('errors.userAlreadyExists')})
      // no user found with these details. Create user
      return User.create({email, password}, function (err, doc) {
        // if err has property errors, there will be validation errors
        if (err) return res.status(400).send(err)
        // No error, 200
        return res.send()
      })
    })
  } else {
    return res.status(400).send({message: req.__('validation.validationFailed')})
  }
})

router.get('/', function (req, res) {
  res.send('Hi from users')
})

module.exports = router
