try {
  // load environment variables from file
  const env = process.env.NODE_ENV || 'development'
  require('dotenv').config({path: __dirname + '/config/' + env + '.env'})
} catch (e) {
  console.error('Could not load ENV file')
  process.exit(1)
}

// mongodb setup
const mongoose = require('mongoose')
// attach to global scope
global.db = mongoose.createConnection(process.env.MONGO_URI)

const express = require('express')
const app = express()
const host = process.env.HOST
const port = process.env.PORT
const helmet = require('helmet')
const cors = require('cors')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const i18n = require('i18n')
const { Nuxt, Builder } = require('nuxt')
const routes = require('./api')

app.set('port', port)

// Import and Set Nuxt.js options
let nuxtConfig = require('../nuxt.config.js')

// CORS options for CSRF protection (only needed for interaction with other servers)
let corsSettings = {}

if (process.env.NODE_ENV !== 'production') {
  // set nuxt to dev for hot reload
  nuxtConfig.dev = true
}

async function start () {
  // Init Nuxt.js
  const nuxt = new Nuxt(nuxtConfig)

  // Build only in dev mode
  if (nuxtConfig.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // configure i18n
  i18n.configure({
    locales: ['en', 'de'],
    defaultLocale: 'en',
    directory: __dirname + '/../locales',
    objectNotation: true,
    register: global
  })
  // security middleware mainly for xss attacks - options: https://github.com/helmetjs/helmet
  app.use(helmet())
  // Cross-Origin Resource Sharing protection
  app.use(cors(corsSettings))
  // parse body to json data
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({extended: true}))
  app.use(cookieParser())
  // internationalization
  app.use(i18n.init)
  app.use('/api', routes)

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  console.log('Server listening on http://' + host + ':' + port) // eslint-disable-line no-console
}
start()
