const Schema = require('mongoose').Schema
const bcrypt = require('bcrypt')
const helpers = require('../../helpers/helpers')

const user = Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    validate: {
      validator: function (v) {
        // validate email format
        return helpers.isValidEmail(v)
      },
      message: 'validation.emailNotValid'
    }
  },
  test: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true,
    validate: {
      validator: function (v) {
        return helpers.isValidPassword(v)
      },
      message: 'validation.passwordStrengthNotMet'
    }
  }
})

user.pre('save', async function (next) {
  try {
    // Generate Salt
    const saltRounds = 10
    const salt = await bcrypt.genSalt(saltRounds)
    // Generate passwordhash with plaintext password and salt
    const passwordHash = await bcrypt.hash(this.password, salt)
    // reassign hashed version of password to user
    this.password = passwordHash
    // make email lowercase
    this.email = this.email.toLowerCase()
    next()
  } catch (error) {
    next(error)
  }
})

user.methods.isValidPassword = async function (enteredPassword) {
  try {
    // compare plaintext password with hash. Bcrypt will return true or false
    return await bcrypt.compare(enteredPassword, this.password)
  } catch (error) {
    throw new Error(error)
  }
}

module.exports = db.model('User', user)
