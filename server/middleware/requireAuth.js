const jwt = require('jsonwebtoken')
const requireAuth = function (req, res, next) {
  // grab JWT off cookies
  const token = req.cookies['authorization']
  if (token) {
    // verify token
    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
      if (err) {
        // token could not be verified
        return res.status(401).send({message: req.__('errors.noSession')})
      } else {
        // token valid - forward
        console.log(decoded)
        next()
      }
    })
  } else {
    // no auth cookie - not logged in
    return res.status(401).send({message: req.__('errors.noSession')})
  }
}

module.exports = requireAuth
