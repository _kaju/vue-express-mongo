import Vue from 'vue'
export default function ({$axios, redirect, from, store, app}) {
  $axios.onRequest(config => {
    console.log('Making request to ' + config.url)
  })

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    const errorMessage = (error.response && error.response.data && error.response.data.message) ? error.response.data.message : app.i18n.t('errors.generic')
    if (code === 401) {
      if (from && from.path !== '/login') redirect('/login')
      store.commit('auth/setError', errorMessage)
    }
  })
}
