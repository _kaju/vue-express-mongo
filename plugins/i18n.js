import Vue from 'vue'
import VueI18n from 'vue-i18n'

class CustomFormatter {
  interpolate (message, values) {
    // regex to find all items in string with 1 or 2 curly braces
    const foundVariables = /{{\S*}}|{\S*}/g
    // replace all found variables with the attributes in the value object by the key of the variablename
    const returnedMessage = message.replace(foundVariables, function (match) {
      // remove curly braces from variablename
      const variableName = match.replace(/{|}/g, '')
      // if the key is found in values, return the value of it, if not return an empty string
      return values && values[variableName] ? values[variableName] : ''
    })
    // expects an array as response so we just attach the constructed string as the first element
    return [returnedMessage]
  }
}

Vue.use(VueI18n)

export default ({app}) => {
  const userLang = navigator.language || navigator.userLanguage
  app.i18n = new VueI18n({
    locale: userLang.substr(0, 2),
    fallbackLocale: 'de',
    formatter: new CustomFormatter(), // comment this out if you need standard functionality of vue-i18n
    messages: {
      'de': require('~/locales/de.json'),
      'en': require('~/locales/en.json')
    }
  })
}
