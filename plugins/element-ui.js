import Vue from 'vue'
import Element from 'element-ui/lib/element-ui.common'
import enLocale from 'element-ui/lib/locale/lang/en'
import deLocale from 'element-ui/lib/locale/lang/de'

export default ({app}) => {
  let userLang = (navigator.language || navigator.userLanguage).substr(0, 2)
  if (userLang === 'de') {
    Vue.use(Element, { locale: deLocale })
  } else {
    Vue.use(Element, { locale: enLocale })
  }
}
